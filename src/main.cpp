#include <iostream>
#include <fstream>
#include <string.h>
#include <iomanip>
#include <chrono>

using namespace std;

int main(int argc, char **argv) {
	cout << "Slowride v1.0" << endl;
	if (argc != 4 && argc != 5) {
		cout << "Usage: sudo ./slowride [block device]  [mb per block] [ms threshold] [optional start GB]" << endl;
		cout << "Example (100mb/s): sudo ./slowride /dev/sda2 100 1000" << endl;
		exit(3);
	}

	auto inputStream = ifstream(argv[1], ifstream::binary);	
	if (!inputStream) {
		cerr << "Error: " << strerror(errno) << endl;
		exit(1);
	}

	inputStream.seekg (0, inputStream.end);
	auto length = inputStream.tellg();
	auto gbs = length / 1024 / 1024 / 1024;

	cout << "Length: " << gbs << "GB" << endl;

	auto bytesBlockSize = 1024;
	auto mgBlocksSize = bytesBlockSize * 1024;
	auto mgbsPerBlock = atoi(argv[2]);

	auto blockSize = mgBlocksSize * mgbsPerBlock;
	auto buffer = new char[blockSize];

	auto timeout = atoi(argv[3]);

	long long int startPosition = 0;
	if (argc == 5) {
		startPosition = atol(argv[4]) * 1024 * 1024 * 1024;
		cout << "Start: " << argv[4] << "GB "<< endl;
	}

	inputStream.seekg (startPosition, inputStream.beg);

	cout << "take it easy" << endl;

	for (long long int position = startPosition; position < length; position += blockSize) {

		auto nextPosition = position + blockSize;
		if (nextPosition > length) {
			blockSize -= nextPosition - length;
		}

		auto start = std::chrono::steady_clock::now(); 
		if (!inputStream.read(buffer, blockSize)) {
			cerr << "Error: " << strerror(errno) << endl;
			exit(2);
		}
		auto end = std::chrono::steady_clock::now();

		if (inputStream.gcount() != blockSize) {
			auto currentGB = position / 1024 / 1024 / 1024;
			cout << "Can't read at " << currentGB << "GB, bad block?" << endl;			
			exit(3);
		}

		auto readTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

		if (readTime > timeout) {
			auto currentGB = position / 1024 / 1024 / 1000;
			cout << "RT:" << currentGB << "GB" << endl;
    			cout << "T: " << fixed  << readTime << setprecision(5); 
			cout << "s" << endl;
		}
		
	}

	inputStream.close();

	cout << "success" << endl;
	return 0;
}